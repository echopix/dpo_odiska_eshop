$(document).ready(function() {
  $('.slick').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});

$(document).ready(function() {
  $('.slickhow').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    arrows: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});

$(document).ready(function(){
	$('.navbar-toggler-icon').click(function(){
		$(this).toggleClass('open');
  });
});

$(document).ready(function(){

  $('#inlineRadio1').click(function(){
    $('#odisnumberwrap').show();
  });

  $('#inlineRadio2').click(function(){
    $('#odisnumberwrap').hide();
  });


});

